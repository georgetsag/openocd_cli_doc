An introduction to openocd, flash programming and debugging
======
Required packages:
 - openocd (>= 0.10.0)
 - libusb
 - st-stlink-server
 - st-stlink-udev-rules
 - compiler toolchain, e.g.: GNU Tools for Arm Embedded Processors 8-2018-q4-major
	  Download [here!](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)

In order to flash succesfully from your CLI, you would need to specify the programming procedure in the `Makefile`, and also provide an `openocd.cfg` file.
For the `qubik-comms-sw` project, this is already done and it is located in the root folder of the repository.

Steps-and-Hints-for-flashing
------

 - Change inside the project root directory where the `openocd.cfg` file is included.
 - Power up your target device
 - Run `openocd` with debug output enabled:

```
$ openocd --debug
```

 - If you see that it's `Initializing PLDs`, open a new terminal and run the debugger:

```
$ arm-none-eabi-gdb
```

After it's launched connect to the remote `gdb` server (`openocd`!), reset the target device and halt it:

```
(gdb) target remote localhost:3333
(gdb) monitor reset halt
```

If you see that target halted due to debug-request, followed by the `xPSR`, `pc`, `msp` values, then everything is set up correctly.

Now you should be able to run `make program` to flash the board.

### Troubleshooting:
 - If you see that the file `stm32l4x.cfg` could not be found, then you most probably have an outdated version of `openocd` installed.
Make sure to get version `0.10.0` or later.
 - If you see that `stlink_usb_open` fails, then check again the target power.

### Extra-notes
 - If you are manually compiling `openocd` (version `0.10.0` or later) make sure to include the `--enable-stlink` option on the configuration step.

### Extra-HW-notes
 - Our current `COMMS` Board (`0.9.3` or higher) supports power supplying through the JST-6 of the STLink debugger hardware, it's deprecated to use the 3V3 from STLink.
It's better to use an EPS and provide power individually.

### Flashing
 - Once your setup has been tested to work you can simply flash a new firmware into the COMMS' processor with executing `make program` on your CLI. It will erase, program and verify the firmware in the current configuration.
 - IDE integration should be easy to do, whether using the `Makefile` as it is or by adding the necessary calls inside your IDE. Use the `Makefile` target `program` as example.

### Debugging
 - If you want to debug your running firmware with a debugger, `gdb` joins the game. There are several ways to use it, e.g. inside your IDE, via graphical debugger like `ddd` or on a bare CLI. Examples for configuring your favorite IDE should be easy to find on all common search engines.
 - Make sure you compile your firmware with the optimization option set to `-Og`, which adds additional information used by the debugger. If you are debugging "production code" parts of the source code might not be existent in the machine code because of optimizations. This is normal behavior and should be carefully considered when testing against a "debug compilation".
 - Inside the root directory of the git repository execute the command `openocd`. It will fetch the configuration stored inside the file `openocd.cfg` and connect to the target device. Now you need to do some basic steps, similar to the ones we used for testing if the STLink debugger hardware works.
 - Inside another terminal execute `arm-none-eabi-gdb` and enter the following commands to connect to the remote target:
   * `file build/qubik-comms-sw.elf`
   * `target remote localhost:3333`
   * `monitor reset halt`

The output should look something like the following:
   
```
$ arm-none-eabi-gdb
GNU gdb (GNU Tools for Arm Embedded Processors 8-2018-q4-major) 8.2.50.20181213-git
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "--host=x86_64-linux-gnu --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb) file build/qubik-comms-sw.elf 
Reading symbols from build/qubik-comms-sw.elf...
(gdb) target remote localhost:3333
Remote debugging using localhost:3333
0x00000000 in ?? ()
(gdb) monitor reset halt
adapter speed: 500 kHz
target halted due to debug-request, current mode: Thread 
xPSR: 0x01000000 pc: 0x08008c24 msp: 0x20018000
(gdb) 
```

 - Now add a breakpoint (`break <symbol>`), i.e., at the entry of the C main function:
 
```
(gdb) break main
Breakpoint 1 at 0x8001000: file Src/main.c, line 129.
(gdb) 
```

 - Use the command (`cont[inue]`) to execute the code on the target until it reaches this first breakpoint:

```
(gdb) cont
Continuing.
Note: automatically using hardware breakpoints for read-only addresses.

Breakpoint 1, main () at Src/main.c:129
129	{
(gdb) 
```

 - The target processor is now stopped at that exact location. If you enter an illegal or not existent function as breakpoint you will get the following:

```
(gdb) break foobar
Function "foobar" not defined.
Make breakpoint pending on future shared library load? (y or [n]) 
```

 - Some further hints on using the debugger:
   * commands can be abbreviated
   * commands can be auto-completed using [TAB]
   * `cont[inue]` starts execution of the remote target
   * `s[tep]` goes to the next instruction diving into function
   * `n[ext]` goes to the next instruction without diving into function
   * `fini[sh]` continues until the current fucntion returns
   * `pri[nt]` display content at address/symbol
 - Further reading in the gdb "cheat sheet" (it is really worth printing it!):
   Download [here](https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf)!
